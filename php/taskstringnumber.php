<?php

/* 
 * author: Danila Dolmatov
 * task:​
 *  Input: 
 *   1. string of characters (String)
 *   2. number of unique characters (Integer)
 *
 * output:
 *  ​​Longest string - starting position 
 */

class Main{
  
  private  $input, $result, $error, $length, $unique_pair, $out;
  public   $output;
  
  /* 
   * input string of characters (String)
   * string of characters (String)
   * number of unique characters (Integer)
   */

  function __construct() {
        
    // POST string & number
    $arg =[
      'str' => FILTER_SANITIZE_STRING,
      'num' => FILTER_VALIDATE_INT
    ];

    $this->input = filter_input_array(INPUT_POST,$arg);

    $this->array_input = str_split(preg_replace( '/[^[:print:]]/', '',$this->input['str']));
    
    $this->length = strlen($this->input['str']);
    
    $result  = $this->checkdata();
    
    $this->task_to_do($result);
    

  }
  
  function checkdata() {
    
    $result = TRUE;
    
    if (empty($this->input['str']) OR 
        empty($this->input['num']) OR 
        ($this->length < $this->input['num'])) {
    
      $result = FALSE;
    } 
    
    return $result;

  }
  
   /* 
   * to do the task
   */
  private function task_to_do ($result) {
    
    if ($result == FALSE) {
    
      $this->error = 'ERROR INPUT DATA';
      
    } else {
    
      // get a unique pair of chars
      $output = $this->get_chars_for_number();

      // get chars from sting
      $this->get_chars_from_string($output);

      // prepare for display data
      $this->count_str_length_position();  
      
    }
    
    // display result
    $this->display();
    
  }
  
  /*
   * get unique chars and limit by num
   */
  private function get_chars_for_number() {
    
    $array_input = $this->array_input;
    
    for ($i = 0; $i < $this->length; $i++) {
        
      $input_unique = array_unique($array_input);
       
      $this->unique_pair[$i] = array_slice($input_unique, 0, $this->input['num']);
        
      array_shift($array_input);
      
      if (count($this->unique_pair[$i]) < $this->input['num']) {
         
        array_pop($this->unique_pair);
        
        break;
        
      }
        
    }

  }
  
  
  /*
   * get chars for condition from string
   */
  private function get_chars_from_string() {
        
    $j = 0;
    
    foreach ($this->unique_pair as $key => $value) {  
      
      for ($i = $j; $i < $this->length; $i++) {

        if (in_array($this->array_input[$i], $value)) {
          
          $this->result[$key][$i] = $this->array_input[$i];         
          
        } else {
          
          break;
          
        }

      }
      
      $j++;
      
    }   
    
  }
  
  /*
   * get max str and starting position
   */
  private function count_str_length_position() {
    
    if (empty($this->result)) {
      
      $this->error = 'WE CAN\'T FIND MATCHES';
      
      return 0;
      
    }
    
    foreach ($this->result as $key => $value) {
            
      $result[$key] = [
                        count($value),
                        'str' => implode('', $value),
                        'position' => $key
                        
                      ];
                       

    }
        
    arsort($result);
    
    $this->out = array_shift($result);

  }


  /*
   * display
   */
  private function display(){
    
    $res['str'] = $this->input['str'];
    
    $res['num'] = $this->input['num'];
    
    $res['success'] = $this->out;
    
    $res['error'] = $this->error;
    
    $this->output = $res;
    
  }


  /* 
   * output result data
   */
  private function output() {
    
    return $this->output;
  
  }

  /* 
   * destructor
   */
  function __destruct() {
     
    $this->output();
    
  }
  
}

/*
 * Create object and display result
 * 
 */
$class = new Main;    
$out = $class->output;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>​Longest string - starting position</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS for BOOTSTRAP -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <h1>Enter data:</h1>
      <form method="POST">
      <div class="form-group">
        <label for="usr">Any characters:</label>
        <input type="text" class="form-control" name="str" required>
      </div>
      <div class="form-group">
        <label for="usr">Number of unique characters:</label>
        <input type="text" class="form-control" type="number" name="num" required>
      </div>
      <div class="row">
        <div class="col-sm-12"><input class="btn" type="submit" value="Submit"></div>
      </div>
      </form>
        <?php if (!empty($out['success'])): ?>
          <h2>input data:</h2>
            <div class="text-muted">​<b>string:</b> <?= $out['str']?></div>
            <div class="text-muted">​<b>​​unique characters:</b> <?= $out['num']?></div>
          <h2>result:</h2>
            <div class="text-success">​<b>​string: </b> <?= $out['success']['str'];?></div>
            <div class="text-success">​<b>length: </b> <?= $out['success'][0];?></div>
            <div class="text-success">​<b>starting position: </b> <?= $out['success']['position'];?> <i>(begin count from 0)</i></div>
        <?php elseif (!empty($out['error'])): ?>
          <h2>ERROR</h2>
          <div class="text-danger">​<?= $out['error']?></div>
        <?php endif; ?>
    </div>
  </body>
</html>