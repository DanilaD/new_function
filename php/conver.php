<?php


// migration

// from table dn053_polling_vals to wp_democracy_a
$migration = 
    [ 
      
      // key - columns from old table 1
      // value - columns to new table 2
      // where key and value - merge data from table 1 to table 2
      'sql' =>
        [
          'qid' => 'pollid',
          'answer' => 'vals_title',
          'votes' => 'vals_voices'
        ],
      
      // new columns for new table 2
      'default' =>
        [
          'aorder' => 0,
          'added_by' => ''
        ],
      
      // name of table new and old
      'table' =>
        [
          'to' => 'wp_democracy_a',
          'from' => 'dn053_polling_vals'
          
        ],
      
      // necessary data to connect to db (new|old)
      'db' =>
        [
          'to' =>
            [
              'host' => 'localhost',
              'user' => 'root',
              'password' => '',
              'db' => 'apby_new',
              'carset' => 'utf8'
            ],
          
          'from' =>
            [
              'host' => 'localhost',
              'user' => 'root',
              'password' => '',
              'db' => 'apby_old',
              'carset' => 'utf8'
            ]
          
        ],
      
    ];


  $class = NEW converData();

  // get data from table FROM and insert in table TO
  $class->getAndSaveData($migration);


class converData{
  
  // mysqli
  private $mysqli;
  
  // file to save data
  private $file = 'export.txt';

  // connect to Db
  private function connectToDb($param) {
    
    $link = mysqli_connect($param['host'], $param['user'], $param['password'], $param['db']);

    if (!$link) {
      
        echo "Error: Unable to connect to MySQL." . PHP_EOL;
        echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
        echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
        exit;
        
    }

    print_r('Success: A proper connection to MySQL was made! The my_db database is great.' . PHP_EOL);
    print_r('Host information: ' . mysqli_get_host_info($link) . PHP_EOL);

    
    $link->set_charset($param['carset']);
    
    $this->mysqli = $link;
  }
  
  // get Data from table 
  private function getFromDb($param) {
    
    // connect to Db
    $this->connectToDb($param['db']['from']);
    
    // get data from table
    return $this->checkDataAndPrepareForQuery($param);

  }
  
  // prepare sql query for selecting data from table
  private function checkDataAndPrepareForQuery($param) {
       
    $select = implode(', ', $param['sql']);
    
    $data = 'SELECT '.$select.' FROM '.$param['table']['from'].'';
    
    print_r('SQL Query: ' .$data);
    
    $result = $this->mysqli->query($data);
    
    if ($result->num_rows < 1) {
      
      echo 'we do not have data in Db: '.$param['table']['from'] ;
      exit;
      
    }
    
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
      
      $rows[] = $row; 
      
    }
    
    return $rows;

  }

  // save data JSON to file
  private function saveIntoFile($array, $param) {
    
    if (!is_array($array)) {
      echo 'we do not have data to save in file';
      exit;
    }
    
    file_put_contents($this->file, json_encode($array));
    
  }
  
  // prepare sql query for inserting in new table
  private function prepareDataForInsertingToTable($array, $param) {
    
    $sql_array = array_flip($param['sql']);
    
    foreach ($array as $value) {
      
      foreach ($value as $k => $v) {
        
        $array_new_key[$sql_array[$k]] = $v;
        
      }      
      
      $merge = (array_merge($array_new_key, $param['default']));
      
      $set_sql[] = $this->implodeForSql($merge, 'values');
      
    }
    
    $set['set'] = $this->implodeForSql($set_sql, 'set');
    
    $array_key = array_keys(array_merge($param['sql'], $param['default']));
    
    $set['key'] = $this->implodeForSql($array_key, 'key');
    
    $this->insertIntoDb($set, $param);
    
  }
  
  // prepare data for VALUE
  private function implodeForSql($array, $type) {
    
    switch ($type) {
      case 'values':
        $imp = '\''.implode('\',\'', $array).'\'';
        break;
      
      case 'key':
        $imp = '('.implode(',', $array).')';
        break;
      
      case 'set':
        $imp = 'VALUES('.implode('), (', $array).')';
        break;

      default:
        $imp = '';
        break;
    }
      
    return $imp;
    
  }
  
  
  private function insertIntoDb($array, $param) {
    
    $sql = 'INSERT INTO '.$param['table']['to'].' '.$array['key'].' '.$array['set'].'';
    
    var_dump($sql);
      
    // connect to Db
      $this->connectToDb($param['db']['to']);
      
    // insert
    if (!$this->mysqli->query($sql)) {
    
      printf("Errormessage: %s\n", $this->mysqli->error);
    
      
    }
    
  }
  
 
  public function getAndSaveData($param) {
    
    // get data from table 1
    
    $array = $this->getFromDb($param);
    
    // save to file
    
  //$this->saveIntoFile($array, $param);
    
    // prepare data for inserting into table 2
    $data = $this->prepareDataForInsertingToTable($array, $param);
    
    // insert to table 2
    
    
  }
}

